var Hand = require('pokersolver').Hand;

// n!/(k!(n-k)!)
const indexHand = [
    [[1, 2, 3, 4, 5]],
    [[1, 2, 3, 4], [2, 3, 4, 5], [1, 2, 4, 5], [1, 2, 3, 5], [1, 2, 3, 5]],
    [[1, 2, 3], [1, 2, 4], [1, 2, 5], [1, 3, 4], [1, 3, 5], [1, 4, 5], [2, 3, 4], [2, 3, 5], [2, 4, 5], [3, 4, 5]],
    [[1, 2], [1, 3], [1, 4], [1, 5], [2, 3], [2, 4], [2, 5], [3, 4], [3, 5], [4, 5]],
    [[1], [2], [3], [4], [5]],
    [[]]
];

class Poker {
    constructor() {

    }

    solver(hand, deck) {
        let bestHand;
        for (let i = 0; i < indexHand.length; i++) {
            for (let j = 0; j < indexHand[i].length; j++) {
                let testHand = [];
                for (let pos = 0; pos < indexHand[i][j].length; pos++) {
                    testHand.push(hand[indexHand[i][j][pos] - 1])
                }
                let fromDeck = deck.length - testHand.length;
                for (let pos = 0; pos < fromDeck; pos++) {
                    testHand.push(deck[pos]);
                }
                let res = Hand.solve(testHand, "standard", false);
                if (bestHand === undefined || bestHand.rank < res.rank) {
                    bestHand = res;
                }
            }
        }
        console.log("Hand: " + hand + " Deck: " + deck + " Best hand: " + bestHand.name)
    }
}

module.exports = Poker;